//
//  ShowVideoPresenter.swift
//  NagarroTask
//
//  Created by Expertapps2 on 30/09/2021.
//  Copyright (c) 2021 ___ORGANIZATIONNAME___. All rights reserved.
//
//  This file was generated by the Clean Swift Xcode Templates so
//  you can apply clean architecture to your iOS and Mac projects,
//  see http://clean-swift.com
//

import UIKit

protocol ShowVideoPresentationLogic {
    func presentVideo(response: ShowVideo.GetVideo.Response)
    func playVideo(url: String)
}

class ShowVideoPresenter: ShowVideoPresentationLogic {
    weak var viewController: ShowVideoDisplayLogic?

    // MARK: - Get order
    func presentVideo(response: ShowVideo.GetVideo.Response) {
        let video = response.video
        let displayedVideo = ShowVideo.GetVideo.ViewModel.DisplayedVideo(id: video.trackID ?? 0,
                                                                         artistName: video.artistName ?? "",
                                                                         trackName: video.trackName ?? "",
                                                                         image: video.artworkUrl100 ?? "",
                                                                         video: video.previewURL ?? "")
        let viewModel = ShowVideo.GetVideo.ViewModel(displayedVideo: displayedVideo)
        viewController?.displayVideo(viewModel: viewModel)
    }
    
    func playVideo(url: String) {
        print(url)
        guard let url = URL(string: url) else {return}
        viewController?.playVideoWithUrl(url: url)
    }
}
