//
//  ShowVideoInteractor.swift
//  NagarroTask
//
//  Created by Expertapps2 on 30/09/2021.
//  Copyright (c) 2021 ___ORGANIZATIONNAME___. All rights reserved.
//
//  This file was generated by the Clean Swift Xcode Templates so
//  you can apply clean architecture to your iOS and Mac projects,
//  see http://clean-swift.com
//

import UIKit

protocol ShowVideoBusinessLogic {
    func getVideo(request: ShowVideo.GetVideo.Request)
    func playVideo()
}

protocol ShowVideoDataStore {
    var video: Video? { get set }
}

class ShowVideoInteractor: ShowVideoBusinessLogic, ShowVideoDataStore {
    var presenter: ShowVideoPresentationLogic?
    var video: Video?
    // MARK: - Get Video
    func getVideo(request: ShowVideo.GetVideo.Request) {
        guard let video = video else { return  }
        let response = ShowVideo.GetVideo.Response(video: video)
        presenter?.presentVideo(response: response)
    }
    
    func playVideo() {
        guard let video = video else { return  }
        presenter?.playVideo(url: video.previewURL ?? "")
    }
}
