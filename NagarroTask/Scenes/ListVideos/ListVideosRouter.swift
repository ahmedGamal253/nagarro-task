//
//  ListVideosRouter.swift
//  NagarroTask
//
//  Created by Expertapps2 on 30/09/2021.
//  Copyright (c) 2021 ___ORGANIZATIONNAME___. All rights reserved.
//
//  This file was generated by the Clean Swift Xcode Templates so
//  you can apply clean architecture to your iOS and Mac projects,
//  see http://clean-swift.com
//

import UIKit

@objc protocol ListVideosRoutingLogic {
    func routeToShowVideo(segue: UIStoryboardSegue?)
}

protocol ListVideosDataPassing {
    var dataStore: ListVideosDataStore? { get }
}

class ListVideosRouter: NSObject, ListVideosRoutingLogic, ListVideosDataPassing {
    weak var viewController: ListVideosViewController?
    var dataStore: ListVideosDataStore?
    
    // MARK: Routing
    func routeToShowVideo(segue: UIStoryboardSegue?) {
        if let segue = segue {
            guard let destinationVC = segue.destination as? ShowVideoViewController else {return}
            guard var destinationDS = destinationVC.router?.dataStore else { return }
            // var destinationDS = destinationVC.router!.dataStore!
            guard let dataStore = dataStore  else { return }
            passDataToShowOrder(source: dataStore, destination: &destinationDS)
        } else {
            guard let destinationVC  =  viewController?.storyboard?
                    .instantiateViewController(withIdentifier: "ShowVideoViewController") as? ShowVideoViewController
            else { return  }
            guard var destinationDS = destinationVC.router?.dataStore else { return }
            //  var destinationDS = destinationVC.router!.dataStore!
            guard let dataStore = dataStore  else { return }
            guard let viewController = viewController else { return }
            passDataToShowOrder(source: dataStore, destination: &destinationDS)
            navigateToShowVideo(source: viewController, destination: destinationVC)
        }
    }
    
    // MARK: Navigation
    func navigateToShowVideo(source: ListVideosViewController, destination: ShowVideoViewController) {
        source.show(destination, sender: nil)
    }
    
    // MARK: Passing data
    func passDataToShowOrder(source: ListVideosDataStore, destination: inout ShowVideoDataStore) {
        guard let selectedRow = viewController?.tableView.indexPathForSelectedRow?.row else {return}
        destination.video = source.videos?[selectedRow]
    }
}
