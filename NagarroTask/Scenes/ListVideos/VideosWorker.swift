//
//  OrdersWorker.swift
//  CleanStore
//
//  Created by Raymond Law on 2/12/19.
//  Copyright © 2019 Clean Swift LLC. All rights reserved.
//

import Foundation

class VideosWorker {
    var videosStore: VideosStoreProtocol
    init(videosStore: VideosStoreProtocol) {
        self.videosStore = videosStore
    }
    
    func fetchVideos(searchKey: String, selectType: String, completionHandler: @escaping ([Video]?, String?) -> Void) {
        videosStore.fetchVideos(searchKey: searchKey, selectType: selectType) { videos, error in
            DispatchQueue.main.async {
                completionHandler(videos, error)
            }
        }
    }
}

// MARK: - Orders store API

protocol VideosStoreProtocol {
    // MARK: CRUD operations - Optional error
    func fetchVideos(searchKey: String, selectType: String, completionHandler: @escaping ([Video]?, String?) -> Void)
}
