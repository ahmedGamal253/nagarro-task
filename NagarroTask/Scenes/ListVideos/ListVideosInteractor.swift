//
//  ListVideosInteractor.swift
//  NagarroTask
//
//  Created by Expertapps2 on 30/09/2021.
//  Copyright (c) 2021 ___ORGANIZATIONNAME___. All rights reserved.
//
//  This file was generated by the Clean Swift Xcode Templates so
//  you can apply clean architecture to your iOS and Mac projects,
//  see http://clean-swift.com
//

import UIKit

protocol ListVideosBusinessLogic {
  func fetchVideos()
  func makeSearchWithKeyword(request: ListVideos.FetchVideos.Request)
  func setSearchTextfildText()
}

protocol ListVideosDataStore {
  var videos: [Video]? { get }
  var selectType: String? { get set}
  var keywordSearch: String? { get set }
}

class ListVideosInteractor: ListVideosBusinessLogic, ListVideosDataStore {
  var presenter: ListVideosPresentationLogic?
  var videosWorker = VideosWorker(videosStore: VideosMemStore())
  var videos: [Video]?
  var selectType: String?
  var keywordSearch: String?
  // MARK: - Fetch Videos
  
    func fetchVideos() {
        presenter?.showIndicator(show: true)
        videosWorker.fetchVideos(searchKey: keywordSearch ?? "",
                                 selectType: selectType ?? "") { [weak self](videos, error) -> Void in
            guard let self = self else {return}
            self.presenter?.showIndicator(show: false)
            if error != nil {
                self.presenter?.showAlart(message: error ?? "Something went wrong")
            } else {
                self.videos = videos
                if videos?.isEmpty ?? false {
                    self.presenter?.showAlart(message: "no video found")
                }
                let response = ListVideos.FetchVideos.Response(videos: videos ?? [])
                self.presenter?.presentFetchedVideos(response: response)
            }
        }
    }
    
    func makeSearchWithKeyword(request: ListVideos.FetchVideos.Request) {
        keywordSearch = request.keywork
        fetchVideos()
    }
    
    func setSearchTextfildText() {
        presenter?.presentTextInSearchTextFild(text: keywordSearch ?? "")
    }
}
