//
//  VideoTableViewCell.swift
//  NagarroTask
//
//  Created by Expertapps2 on 30/09/2021.
//

import UIKit

class VideoTableViewCell: UITableViewCell {

    @IBOutlet weak var lblArtName: UILabel!
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var imgVideo: UIImageView!
    @IBOutlet weak var viewCell: UIView!

    override func awakeFromNib() {
        super.awakeFromNib()
        initUi()
        // Initialization code
    }

    private func initUi() {
        viewCell.layer.cornerRadius = 10
        imgVideo.layer.cornerRadius = 8
    }
    
    func configCell(_ model: ListVideos.FetchVideos.ViewModel.DisplayedVideo) {
        lblName.text = model.trackName
        lblArtName.text = model.artistName
        lblPrice.text = model.price
        imgVideo.loadFromUrl(from: model.image)
    }

}
