//

import Foundation

class VideosMemStore: VideosStoreProtocol {
   
  // MARK: - CRUD operations - Optional error
  
    func fetchVideos(searchKey: String,
                     selectType: String,
                     completionHandler: @escaping ([Video]?, String?) -> Void) {
        let type = selectType.removeWhitespace()
        let urlString = "https://itunes.apple.com/search/\(type)?term=\(searchKey)"
        guard let url = URL(string: urlString) else {
            completionHandler(nil, "Cannot fetch videos")
            return
        }
       // let url = URL(string: urlString)
        let task = URLSession.shared.dataTask(with: url) { data, _, error in
            guard let data = data, error == nil else {
                completionHandler(nil, "Cannot fetch videos")
                return
            }
            do {
                guard let jsonDic = try JSONSerialization
                        .jsonObject(with: data, options: []) as? [String: AnyObject] else {
                    completionHandler(nil, "Cannot fetch videos")
                    return
                }
                let jsonData = try JSONSerialization.data(withJSONObject: jsonDic)
                let model = try JSONDecoder().decode(VideosResponseModel.self, from: jsonData)
                completionHandler(model.results, nil)
            } catch {
                completionHandler(nil, "Cannot fetch videos")
            }
        }
        task.resume()
    }
}
