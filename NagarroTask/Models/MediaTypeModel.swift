//
//  MediaTypeModel.swift
//  NagarroTask
//
//  Created by Expertapps2 on 03/11/2021.
//

import Foundation

class MediaTypeModel {
    var name: String = ""
    var select: Bool = false
    
    init(name: String, select: Bool = false) {
        self.name = name
        self.select = select
    }
}
