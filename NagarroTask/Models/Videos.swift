//
//  Videos.swift
//  NagarroTask
//
//  Created by Expertapps2 on 30/09/2021.
//

import Foundation

// MARK: - VideosResponseModel
struct VideosResponseModel: Codable {
    let resultCount: Int?
    let results: [Video]?
}

// MARK: - Result
struct Video: Codable, Equatable {
    let wrapperType: String?
    let kind: String?
    let artistID, trackID: Int?
    let artistName, trackName: String?
    let trackCensoredName: String?
    let artistViewURL, trackViewURL: String?
    let previewURL: String?
    let artworkUrl30, artworkUrl60, artworkUrl100: String?
    let collectionPrice, trackPrice: Double?
    let releaseDate: String?
    let trackTimeMillis: Int?
    let country: Country?
    let currency: Currency?
    let primaryGenreName: String?
    
    enum CodingKeys: String, CodingKey {
        case wrapperType, kind
        case artistID = "artistId"
        case trackID = "trackId"
        case artistName, trackName, trackCensoredName
        case artistViewURL = "artistViewUrl"
        case trackViewURL = "trackViewUrl"
        case previewURL = "previewUrl"
        case artworkUrl30,
             artworkUrl60,
             artworkUrl100,
             collectionPrice,
             trackPrice, releaseDate, trackTimeMillis, country, currency, primaryGenreName
    }
}

enum Country: String, Codable {
    case usa = "USA"
}

enum Currency: String, Codable {
    case usd = "USD"
}
