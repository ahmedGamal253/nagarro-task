//
//  UIViewControllerExtenions.swift
//  NagarroTask
//
//  Created by Expertapps2 on 01/10/2021.
//

import Foundation
import UIKit

extension UIViewController {
    func showAlart(message: String) {
        let alert = UIAlertController(title: "Alert", message: message, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
}
