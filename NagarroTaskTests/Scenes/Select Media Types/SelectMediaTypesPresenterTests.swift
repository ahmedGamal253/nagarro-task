//
//  SelectMediaTypesPresenterTests.swift
//  NagarroTask
//
//  Created by Expertapps2 on 04/11/2021.
//  Copyright (c) 2021 ___ORGANIZATIONNAME___. All rights reserved.
//
//  This file was generated by the Clean Swift Xcode Templates so
//  you can apply clean architecture to your iOS and Mac projects,
//  see http://clean-swift.com
//

@testable import NagarroTask
import XCTest

class SelectMediaTypesPresenterTests: XCTestCase {
  // MARK: Subject under test
  
  var sut: SelectTypePresenter!
  
  // MARK: Test lifecycle
  
  override func setUp() {
    super.setUp()
    setupSelectMediaTypesPresenter()
  }
  
  override func tearDown() {
    super.tearDown()
  }
  
  // MARK: Test setup
  
  func setupSelectMediaTypesPresenter() {
    sut = SelectTypePresenter()
  }
  
  // MARK: Test doubles
  
    class SelectMediaTypesDisplayLogicSpy: SelectTypeDisplayLogic {
        var displayMediaTypeCalled = false
        var dismisViewWithSelectedTypesCalled = false
        var viewModel: SelectType.FetchTypeMedia.ViewModel!
        
        func displayMediaType(viewModel: SelectType.FetchTypeMedia.ViewModel) {
            displayMediaTypeCalled = true
            self.viewModel = viewModel
        }
        
        func dismisViewWithSelectedTypes() {
            dismisViewWithSelectedTypesCalled = true
        }
  }
  
  // MARK: Tests
  
    func testPresentFetcheTypesShouldFormatFetchedTypessForDisplay() {
      // Given
      let listTypesDisplayLogicSpy = SelectMediaTypesDisplayLogicSpy()
      sut.viewController = listTypesDisplayLogicSpy
      
      // When
      
      let album = Seeds.MediaTypes.album
      let types = [album]
      
      let response = SelectType.FetchTypeMedia.Response(types: types, selectTypes: "album")
      sut.presentMediaTypes(response: response)
      
      // Then
      let displayedTypes = listTypesDisplayLogicSpy.viewModel.displayMediaTypes
      for displayedType in displayedTypes {
        XCTAssertEqual(displayedType.name, "Album", "Presenting type should properly format type name")
        XCTAssertEqual(displayedType.select, false,
                       "Presenting type should properly type select")
      }
    }
    
    func testPresentFetchedVideosShouldAskViewControllerToDisplayFetchedVideos() {
      // Given
      let listTypeDisplayLogicSpy = SelectMediaTypesDisplayLogicSpy()
      sut.viewController = listTypeDisplayLogicSpy
      
      // When
      let types = [Seeds.MediaTypes.album]
      let response = SelectType.FetchTypeMedia.Response(types: types, selectTypes: "album")
      sut.presentMediaTypes(response: response)
      // Then
      XCTAssert(listTypeDisplayLogicSpy.displayMediaTypeCalled,
                "Presenting fetched videos should ask view controller to display them")
    }
    
    func testPresentDismissViewShouldAskViewControllerToDismissView() {
      // Given
      let listTypeDisplayLogicSpy = SelectMediaTypesDisplayLogicSpy()
      sut.viewController = listTypeDisplayLogicSpy
      
      // When
      let types = [Seeds.MediaTypes.album]
      let response = SelectType.FetchTypeMedia.Response(types: types, selectTypes: "album")
      sut.presentSelectedMediaTypes(response: response)
      // Then
      XCTAssert(listTypeDisplayLogicSpy.dismisViewWithSelectedTypesCalled,
                "Presenting fetched videos should ask view controller to display them")
    }
}
