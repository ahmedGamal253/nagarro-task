@testable import NagarroTask
import XCTest

struct Seeds {
  struct Videos {
    
    static let test1 = Video(wrapperType: "track",
                             kind: "music-video",
                             artistID: 1,
                             trackID: 1,
                             artistName: "Miley Cyrus",
                             trackName: "Can't Be Tamed" ,
                             trackCensoredName: "asd",
                             artistViewURL: "asd",
                             trackViewURL: "asd",
                             previewURL: "asd",
                             artworkUrl30: "asd",
                             artworkUrl60: "asd",
                             artworkUrl100: "asd",
                             collectionPrice: 12.5,
                             trackPrice: 23.5,
                             releaseDate: "asd",
                             trackTimeMillis: 123,
                             country: Country.usa,
                             currency: Currency.usd,
                             primaryGenreName: "asd")
    }
    
    struct MediaTypes {
        static let album = MediaTypeModel(name: "Album")
    }
}
